package uz.pdp.app_cinema.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.app_cinema.model.SessionTime;
import uz.pdp.app_cinema.projection.SessionTimeProjection;

import java.util.List;
import java.util.UUID;

public interface SessionTimeRepository extends JpaRepository<SessionTime, UUID> {

    @Query(value = "select distinct cast(st.id as varchar) as id,\n" +
            "                cast(ms.id as varchar) as sessionId,\n" +
            "                time\n" +
            "from session_time st\n" +
            "         join movies_sessions ms on st.id = ms.start_time_id\n" +
            "where ms.halls_id = :hallId\n" +
            "  and movie_announcement_id = :movieAnnouncementId\n" +
            "  and ms.start_date_id = :startDateId", nativeQuery = true)
    List<SessionTimeProjection> getTimesByHallIdAndAnnouncementIdAndStartDateId(UUID hallId, UUID movieAnnouncementId, UUID startDateId);
}
