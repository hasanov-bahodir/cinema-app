package uz.pdp.app_cinema.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.app_cinema.model.Seat;
import uz.pdp.app_cinema.projection.AvailableSeatProjection;
import uz.pdp.app_cinema.projection.AvailableSeatsProjection;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface SeatsRepository extends JpaRepository<Seat, UUID> {

    @Query(value = "select cast(s.id as varchar) as id ,\n" +
            "       s.number as number,\n" +
            "       cast(s.price_category_id as varchar) as priceCategoryId,\n" +
            "       pc.name as priceCategoryName,\n" +
            "       pc.color as color\n" +
            "from seats s join hall_rows hr on s.row_id = hr.id\n" +
            "join price_categories pc on s.price_category_id = pc.id\n" +
            "where hr.id=:rowId", nativeQuery = true)
    List<AvailableSeatProjection> getAllByRowId(UUID rowId);

    @Query(value = "select cast(h.id as varchar) as id,\n" +
            "       h.name              as Name \n" +
            "from halls h\n" +
            "join movies_sessions ms on h.id = ms.halls_id\n" +
            "where ms.id=:movieSessionId", nativeQuery = true)
    Optional<AvailableSeatsProjection> findAllAvailableSeatsByMovieSessionId(UUID movieSessionId);
}
