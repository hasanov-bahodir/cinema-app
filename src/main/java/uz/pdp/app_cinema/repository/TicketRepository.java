package uz.pdp.app_cinema.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.pdp.app_cinema.model.Ticket;
import uz.pdp.app_cinema.model.enums.TicketStatus;
import uz.pdp.app_cinema.projection.TicketProjection;

import java.util.List;
import java.util.UUID;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, UUID> {

    @Query(value = "select cast(t.id as varchar) as id,\n" +
            "       m.title,\n" +
            "       cast(ms.id as varchar) as sessionId,\n" +
            "       s.number as seatNumber,\n" +
            "       r.number as rowNumber,\n" +
            "       t.price as price,\n" +
            "       sd.date as sessionDate,\n" +
            "       st.time as sessionTime\n" +
            "from tickets t join seats s on t.seat_id = s.id\n" +
            "    join hall_rows r on s.row_id = r.id\n" +
            "    join halls h on r.hall_id = h.id\n" +
            "    join movies_sessions ms on t.movie_session_id = ms.id\n" +
            "join movies_announcements ma on ma.id = ms.movie_announcement_id\n" +
            "join movies m on m.id = ma.movie_id\n" +
            "join session_date sd on ms.start_date_id = sd.id\n" +
            "join session_time st on ms.end_time_id = st.id where t.user_id = :userId", nativeQuery = true)
    List<TicketProjection> getTicketByUserId(UUID userId);

    @Query(nativeQuery = true, value = "select t.* from tickets t where t.user_id=:userId and t.status='NEW'")
    List<Ticket> findTicketsByUserIdAndStatusNew(UUID userId);

    Ticket findTicketById(UUID ticketID);
}
