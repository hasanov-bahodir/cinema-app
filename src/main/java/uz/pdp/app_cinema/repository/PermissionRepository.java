package uz.pdp.app_cinema.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.app_cinema.model.Permission;

import java.util.UUID;

public interface PermissionRepository extends JpaRepository<Permission, UUID> {
}
