package uz.pdp.app_cinema.repository;

// Bahodir Hasanov 3/15/2022 4:28 PM

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.app_cinema.model.Attachment;

import java.util.UUID;

public interface AttachmentRepository extends JpaRepository<Attachment, UUID> {


}
