package uz.pdp.app_cinema.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.app_cinema.model.Hall;
import uz.pdp.app_cinema.projection.HallAndTimesProjectionForSession;
import uz.pdp.app_cinema.projection.HallProjection;

import java.util.List;
import java.util.UUID;

// Bahodir Hasanov 3/15/2022 9:34 AM

public interface HallRepository extends JpaRepository<Hall, UUID> {
    @Query(value = "select cast(h.id as varchar) as id, " +
            "h.name ," +
            "       cast(ms.start_date_id as varchar) as startDateId,\n" +
            "       cast(movie_announcement_id as varchar) as movieAnnouncementId\n" +
            "from halls h\n" +
            "         join movies_sessions ms on h.id = ms.halls_id\n" +
            "where ms.start_date_id=:startDateId and movie_announcement_id=:movieAnnouncementId", nativeQuery = true)
    List<HallAndTimesProjectionForSession> getHallsAndTimesByMovieAnnouncementIdAndStartDateId(UUID movieAnnouncementId, UUID startDateId);
}
