package uz.pdp.app_cinema.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.app_cinema.model.SessionDate;
import uz.pdp.app_cinema.model.SessionTime;

import java.util.UUID;

public interface SessionDateRepository extends JpaRepository<SessionDate, UUID> {

}
