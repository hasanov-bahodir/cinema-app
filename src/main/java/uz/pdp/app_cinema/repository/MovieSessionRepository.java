package uz.pdp.app_cinema.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.app_cinema.model.MovieSession;
import uz.pdp.app_cinema.projection.MovieSessionProjection;

import java.util.UUID;

public interface MovieSessionRepository extends JpaRepository<MovieSession, UUID> {
    @Query(value = "select cast(ma.id as varchar)            as movieAnnouncementId,\n" +
            "       cast(ma.id as varchar)            as movieId,\n" +
            "       m.title                           as movieTitle,\n" +
            "       cast(m.cover_image_id as varchar) as movieCoverImgId,\n" +
            "       cast(sd.id as varchar)            as startDateId,\n" +
            "       sd.date                               as startDate\n" +
            "from movies_sessions ms\n" +
            "         join movies_announcements ma on ms.movie_announcement_id = ma.id\n" +
            "         join movies m on ma.movie_id = m.id\n" +
            "         join session_date sd on ms.start_date_id = sd.id\n" +
            "where m.title ilike concat('%',:search,'%') and sd.date>= cast(now() as date) order by sd.date",nativeQuery = true)
    Page<MovieSessionProjection> findAllSessionsByPage(Pageable pageable, String search);
}
