package uz.pdp.app_cinema.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.app_cinema.model.Hall;

import java.util.UUID;
@Projection(types={Hall.class})

public interface HallProjection {
    UUID getId();
    String getName();
}
