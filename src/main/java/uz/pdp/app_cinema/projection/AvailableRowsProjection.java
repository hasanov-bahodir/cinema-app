package uz.pdp.app_cinema.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import uz.pdp.app_cinema.model.Row;

import java.util.List;
import java.util.UUID;

// Bahodir Hasanov 3/15/2022 9:51 AM
@Projection(types = {Row.class})
public interface AvailableRowsProjection {
    UUID getId();
    Integer getNumber();
    @Value("#{@seatsRepository.getAllByRowId(target.id)}")
    List<AvailableSeatProjection> getSeats();
}
