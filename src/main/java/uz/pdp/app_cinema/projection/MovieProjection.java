package uz.pdp.app_cinema.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.app_cinema.model.Movie;

import java.time.LocalDate;
import java.util.UUID;

@Projection(types = {Movie.class})
public interface MovieProjection {
    UUID getId();
    String getTitle();
    String getCoverImageId();
    LocalDate getReleaseDate();
}
