package uz.pdp.app_cinema.projection;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.UUID;

public interface TicketProjection {
    UUID getId();
    String getTitle();
    UUID getSessionId();
    Integer getSeatNumber();
    Integer getRowNumber();
    Double getPrice();
    LocalDate getSessionDate();
    LocalTime getSessionTime();


}
