package uz.pdp.app_cinema.config;
// Bahodir Hasanov 4/6/2022 8:44 AM

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import uz.pdp.app_cinema.service.AuthService;

import static uz.pdp.app_cinema.model.enums.PermissionName.*;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    AuthService authService;

    public AuthenticationProvider getAuthProvider() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setUserDetailsService(authService);
        provider.setPasswordEncoder(returnPasswordEncoder());
        return provider;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
//                .antMatchers(HttpMethod.POST, "/api/movie")
//                    .hasRole("ADMIN")
                .antMatchers("/api/movie", "/")
                .permitAll()
//                    .hasRole("ADMIN")
//                    .authenticated()
//                .antMatchers("/api/**")
//                    .permitAll()
                .anyRequest()
                .authenticated()
                .and()
                .oauth2Login()
                    .defaultSuccessUrl("/success-url-oauth", true)
                    .failureUrl("/error")
                    .loginPage("/oauth_login").permitAll()
                .and()
                .formLogin()
                    .defaultSuccessUrl("/success-url",true)
                    .failureUrl("/error")
                    .loginPage("/oauth_login").permitAll()
                .and()
                .httpBasic();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(getAuthProvider());
    }

    @Bean
    public PasswordEncoder returnPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
