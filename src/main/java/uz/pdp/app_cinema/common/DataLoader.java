package uz.pdp.app_cinema.common;

// Bahodir Hasanov 3/15/2022 9:32 AM

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import uz.pdp.app_cinema.model.*;
import uz.pdp.app_cinema.model.enums.Gender;
import uz.pdp.app_cinema.model.enums.PermissionName;
import uz.pdp.app_cinema.repository.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

@Component
public class DataLoader implements CommandLineRunner {
    @Autowired
    PasswordEncoder passwordEncoder;

    @Value("${spring.sql.init.mode}")
    String initMode;

    final UserRepository userRepository;
    final RoleRepository roleRepository;
    final PermissionRepository permissionRepository;
    final RowRepository rowRepository;
    final HallRepository hallRepository;
    final AttachmentRepository attachmentRepository;
    final MovieRepository movieRepository;
    final SessionDateRepository sessionDateRepository;
    final SessionTimeRepository sessionTimeRepository;
    final MovieAnnouncementRepository movieAnnouncementRepository;
    final MovieSessionRepository movieSessionRepository;
    final PriceCategoryRepository priceCategoryRepository;

    public DataLoader(
            UserRepository userRepository, RoleRepository roleRepository, PermissionRepository permissionRepository, RowRepository rowRepository,
            HallRepository hallRepository,
            AttachmentRepository attachmentRepository,
            MovieRepository movieRepository,
            SessionDateRepository sessionDateRepository,
            SessionTimeRepository sessionTimeRepository,
            MovieAnnouncementRepository movieAnnouncementRepository,
            MovieSessionRepository movieSessionRepository,
            PriceCategoryRepository priceCategoryRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.permissionRepository = permissionRepository;
        this.rowRepository = rowRepository;
        this.hallRepository = hallRepository;
        this.attachmentRepository = attachmentRepository;
        this.movieRepository = movieRepository;
        this.sessionDateRepository = sessionDateRepository;
        this.sessionTimeRepository = sessionTimeRepository;
        this.movieAnnouncementRepository = movieAnnouncementRepository;
        this.movieSessionRepository = movieSessionRepository;
        this.priceCategoryRepository = priceCategoryRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        if (initMode.equals("always")) {

            // CREATE PRICE CATEGORY LIST
            List<PriceCategory> priceCategories = new ArrayList<>();
            for (int i = 0; i < 4; i++) {
                priceCategories.add(new PriceCategory(
                        "Cat" + (i + 1),
                        (2.0 * i),
                        "#" + i + i + i
                ));
            }
            priceCategoryRepository.saveAll(priceCategories);


            // HALL OBJECT CREATE
            Hall zal1 = new Hall("Zal 1");
            Hall zal2 = new Hall("Zal 2");
            Hall zal3Vip = new Hall("Zal 3 VIP", 20.0);


            // ROW OBJECT CREATE

            //================ZAL1 ROWS=====================
            List<Row> rowList1 = new ArrayList<>();
            for (int i = 0; i < 5; i++) {
                Row rowN = new Row(i + 1, zal1);
                //SEAT LIST CREATE
                List<Seat> seats = new ArrayList<>();
                for (int j = 0; j < 10; j++) {
                    int priceCatIndex = j < 3 ? 1 : j < 6 ? 2 : 3;
                    seats.add(new Seat(j + 1, rowN, priceCategories.get(priceCatIndex)));
                }
                rowN.setSeats(seats);
                rowList1.add(rowN);
            }
            //=================ZAL2 ROWS====================
            List<Row> rowList2 = new ArrayList<>();
            for (int i = 0; i < 5; i++) {
                Row rowN = new Row(i + 1, zal2);
                //SEAT LIST CREATE QILINADI
                List<Seat> seats = new ArrayList<>();
                for (int j = 0; j < 10; j++) {
                    int priceCatIndex = j < 3 ? 1 : j < 6 ? 2 : 3;
                    seats.add(new Seat(j + 1, rowN, priceCategories.get(priceCatIndex)));
                }
                rowN.setSeats(seats);
                rowList2.add(rowN);
            }
            //=================ZAL3VIP ROWS====================
            List<Row> rowList3 = new ArrayList<>();
            for (int i = 0; i < 5; i++) {
                Row rowN = new Row(i + 1, zal3Vip);
                //SEAT LIST CREATE
                List<Seat> seats = new ArrayList<>();
                for (int j = 0; j < 10; j++) {
                    int priceCatIndex = j < 3 ? 1 : j < 6 ? 2 : 3;
                    seats.add(new Seat(j + 1, rowN, priceCategories.get(priceCatIndex)));
                }
                rowN.setSeats(seats);
                rowList3.add(rowN);
            }


            //ROW LIST CREATE (IS WAS FIRST TIME THEN WE ADDED ROW WITH SEATS)
//            List<Row> rowList1 = new ArrayList<>();
//            for (int i = 0; i < 10; i++) {
//                rowList1.add(new Row(i + 1, zal1));
//            }
//            List<Row> rowList2 = new ArrayList<>();
//            for (int i = 0; i < 10; i++) {
//                rowList2.add(new Row(i + 1, zal2));
//            }
//            List<Row> rowList3 = new ArrayList<>();
//            for (int i = 0; i < 10; i++) {
//                rowList3.add(new Row(i + 1, zal3Vip));
//            }

            zal1.setRows(rowList1);
            zal2.setRows(rowList2);
            zal3Vip.setRows(rowList3);

            // TO SAVE ZAL1, ZAL2, ZAL3VIP TO DB
            hallRepository.save(zal1);
            hallRepository.save(zal2);
            hallRepository.save(zal3Vip);


            //attachment img
            Attachment movieImg = attachmentRepository.save(new Attachment("movieImg", 100000l, "image/jpg"));
            // MOVIES

            Movie movie1 = new Movie("Batman", "dsgagadsgasgasdg", 120, 50000, movieImg, "youtube.com", LocalDate.now(), 10000000.0, null, 50.0, null, null);
            Movie movie2 = new Movie("Spiderman", "zxcvzxcv cbvxvxcbxxcv dgfshdfghdfghfg", 110, 40000, movieImg, "youtube.com", LocalDate.now(), 9000000.0, null, 40.0, null, null);
            Movie movie3 = new Movie("Superman", "xzcvzcx teyrtyuru bxcxbvcx", 90, 45000, movieImg, "youtube.com", LocalDate.now(), 12000000.0, null, 60.0, null, null);
            movieRepository.save(movie1);
            Movie spiderman = movieRepository.save(movie2);
            Movie superman = movieRepository.save(movie3);

            // SESSION DATES
            SessionDate march17 = new SessionDate(LocalDate.of(2022, 4, 7));
            SessionDate march18 = new SessionDate(LocalDate.of(2022, 4, 8));
            SessionDate march19 = new SessionDate(LocalDate.of(2022, 4, 9));
            sessionDateRepository.save(march17);
            sessionDateRepository.save(march18);
            sessionDateRepository.save(march19);

            //SESSION TIMES
            SessionTime hour11 = new SessionTime(LocalTime.of(11, 0));
            SessionTime hour13 = new SessionTime(LocalTime.of(13, 0));
            SessionTime hour15 = new SessionTime(LocalTime.of(15, 0));
            SessionTime hour18 = new SessionTime(LocalTime.of(18, 0));
            sessionTimeRepository.save(hour11);
            sessionTimeRepository.save(hour13);
            sessionTimeRepository.save(hour15);
            sessionTimeRepository.save(hour18);

            //MOVIE ANNOUNCEMENTS
            MovieAnnouncement batmanAfisha = movieAnnouncementRepository.save(
                    new MovieAnnouncement(movie1, true));
            MovieAnnouncement spidermanAfisha = movieAnnouncementRepository.save(
                    new MovieAnnouncement(spiderman, true));
            MovieAnnouncement supermanAfisha = movieAnnouncementRepository.save(
                    new MovieAnnouncement(superman, true));

            // MOVIE SESSIONS

            movieSessionRepository.save(
                    new MovieSession(
                            batmanAfisha,
                            zal1,
                            march18,
                            hour11,
                            hour13
                    )
            );
            movieSessionRepository.save(
                    new MovieSession(
                            batmanAfisha,
                            zal1,
                            march18,
                            hour15,
                            hour18
                    )
            );
            movieSessionRepository.save(
                    new MovieSession(
                            spidermanAfisha,
                            zal3Vip,
                            march18,
                            hour15,
                            hour18
                    )
            );

            movieSessionRepository.save(
                    new MovieSession(
                            spidermanAfisha,
                            zal2,
                            march19,
                            hour11,
                            hour13
                    )
            );
            movieSessionRepository.save(
                    new MovieSession(
                            spidermanAfisha,
                            zal2,
                            march19,
                            hour15,
                            hour18
                    )
            );

            movieSessionRepository.save(
                    new MovieSession(
                            supermanAfisha,
                            zal3Vip,
                            march19,
                            hour11,
                            hour13
                    )
            );
            Permission permission = new Permission(PermissionName.CAN_ADD_MOVIE.name());
            Permission permission2 = new Permission(PermissionName.CAN_ADD_HOLE.name());
            Set<Permission> permissions = new HashSet<>();
            permissions.add(permission);
            permissions.add(permission2);

            Role admin = roleRepository.save(new Role("ROLE_ADMIN", permissions));
            Role user = roleRepository.save(new Role("ROLE_USER", null));


            userRepository.save(new User(
                    "Admin",
                    "admin",
                    passwordEncoder.encode("1"),
                    "hasanovbahodirq@gmail.com",
                    "+998903312161",
                    LocalDate.of(1995, 11, 03),
                    Gender.MALE,
                    Collections.singleton(admin)
            ));
            userRepository.save(new User(
                    "User",
                    "user",
                    passwordEncoder.encode("1"),
                    "ab200hasanov@gmail.com",
                    "+998907777777",
                    LocalDate.of(20, 3, 21),
                    Gender.MALE,
                    Collections.singleton(user)
            ));

        }
    }
}
