package uz.pdp.app_cinema.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.app_cinema.model.Row;
import uz.pdp.app_cinema.projection.RowProjection;
import uz.pdp.app_cinema.repository.RowRepository;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

// Bahodir Hasanov 3/15/2022 2:09 PM
@Service
public class RowService {
    @Autowired
    RowRepository rowRepository;

    public List<Row> getAllRows() {
        Collection<Row> all = rowRepository.findAll();
        if (all.isEmpty()) {
            return null;
        } else {
            return (List<Row>) all;
        }
    }

    public List<RowProjection> getAllRowsById(UUID id) {
        List<RowProjection> all = rowRepository.getRowsByHallId(id);
        if (all.isEmpty()) {
            return null;
        } else {
            return all;
        }

    }
}
