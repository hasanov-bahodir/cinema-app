package uz.pdp.app_cinema.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.pdp.app_cinema.dto.HallDto;
import uz.pdp.app_cinema.model.Hall;
import uz.pdp.app_cinema.payload.ApiResponse;
import uz.pdp.app_cinema.repository.HallRepository;

import java.util.Collection;
import java.util.List;

// Bahodir Hasanov 3/26/2022 10:23 AM
@Service
@Transactional
public class HallService {
    @Autowired
    HallRepository hallRepository;


    public ResponseEntity<?> savaHall(HallDto hallDto) {
        try {
            Hall saveHall = hallRepository.save(new Hall(hallDto.getName(), hallDto.getVipAdditionalFeeInPercent()));
            return ResponseEntity.ok(new ApiResponse("successfully added", true,saveHall));
        } catch (Exception e){
          return ResponseEntity.internalServerError().body(new ApiResponse("error",false));
        }
    }

    public ResponseEntity<?> getAllHall() {
        try{
            List<Hall> halls = hallRepository.findAll();
            return ResponseEntity.ok(new ApiResponse("success",true,halls));
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ApiResponse("success",false));
        }
    }
}
