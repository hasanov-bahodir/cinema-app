package uz.pdp.app_cinema.service;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.app_cinema.model.Attachment;
import uz.pdp.app_cinema.model.AttachmentContent;
import uz.pdp.app_cinema.payload.ApiResponse;
import uz.pdp.app_cinema.repository.AttachmentContentRepository;
import uz.pdp.app_cinema.repository.AttachmentRepository;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;

// Bahodir Hasanov 3/15/2022 4:27 PM
@Service
@Transactional
public class AttachmentService {
    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired
    AttachmentContentRepository attachmentContentRepository;

    public ApiResponse addAttachment(MultipartFile file) {
        try {
            Attachment attachment = attachmentRepository.save(new Attachment(file.getContentType(), file.getSize(), file.getOriginalFilename()));
            AttachmentContent attachmentContent = attachmentContentRepository.save(new AttachmentContent(attachment, file.getBytes()));

            return new ApiResponse("Good job", true, attachment);
        } catch (Exception e) {
            return new ApiResponse("Oops", false);
        }
    }

    public ApiResponse getAllAttachment() {
        List<Attachment> allAttachment = attachmentRepository.findAll();
        if (allAttachment.isEmpty()) {
            return new ApiResponse("Not found", false);
        }
        return new ApiResponse("Found", true, allAttachment);

    }

    public ResponseEntity<?> getAttachmentById(UUID id) {
        Optional<Attachment> byId = attachmentRepository.findById(id);
        if (byId.isPresent()) {
            return ResponseEntity.ok(byId);
        }
        return ResponseEntity.notFound().build();
    }

    public ApiResponse updateAttachment(UUID id, MultipartFile file) {
        Optional<Attachment> attachmentWithId = attachmentRepository.findById(id);
        if (attachmentWithId.isPresent()) {
            Attachment attachment = attachmentWithId.get();
            attachment.setSize(file.getSize());
            attachment.setName(file.getOriginalFilename());
            attachment.setContent_type(file.getContentType());
            Attachment savedAttachment = attachmentRepository.save(attachment);
            AttachmentContent byAttachmentId = attachmentContentRepository.findByAttachmentId(id);
            byAttachmentId.setAttachment(attachment);
            try {
                byAttachmentId.setBytes(file.getBytes());
                attachmentContentRepository.save(byAttachmentId);
                return new ApiResponse("successfully", true, savedAttachment);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return new ApiResponse("excuse Me!!!", false);
    }

    public ApiResponse deleteAttachment(UUID id) {
        Optional<Attachment> attachment = attachmentRepository.findById(id);
        if (attachment.isPresent()) {
            try {
                attachmentRepository.deleteById(id);
                return new ApiResponse("deleted", true);
            } catch (Exception e) {
                return new ApiResponse("You cannot delete this photo", false);
            }
        }
        return new ApiResponse("Not found", false);
    }

    public Attachment getTicketWithQR(UUID ticketId) {
        /**
         * this line code to write qr code to the resource package
         */
        //  String path = "E:\\PDPJavaUnicornB7\\Sixth Module\\app_cinema\\src\\main\\resources\\name.png";
        String charset = "UTF-8";
        String date = ticketId.toString();
        int width = 200;
        int height = 200;
        Map<EncodeHintType, ErrorCorrectionLevel> hashMap = new HashMap<EncodeHintType, ErrorCorrectionLevel>();
        hashMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
        try {
            BitMatrix bitMatrix = new MultiFormatWriter().encode(new String(date.getBytes(charset), charset),
                    BarcodeFormat.QR_CODE, width, height);
            // MatrixToImageWriter.writeToFile(bitMatrix, path.substring(path.lastIndexOf('.') + 1), new File(path));
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ImageIO.write(MatrixToImageWriter.toBufferedImage(bitMatrix), "png", byteArrayOutputStream);
            byte[] bytes = byteArrayOutputStream.toByteArray();
            Attachment attachment = new Attachment("image/png", 20L, UUID.randomUUID().toString());
             Attachment attachment1 = attachmentRepository.save(attachment);
            AttachmentContent attachmentContent = attachmentContentRepository.save(new AttachmentContent(attachment1, bytes));
            // BufferedImage bufferedImage = MatrixToImageWriter.toBufferedImage(bitMatrix);
            return attachment1;
        } catch (Exception e) {
            return null;
        }
    }
}
