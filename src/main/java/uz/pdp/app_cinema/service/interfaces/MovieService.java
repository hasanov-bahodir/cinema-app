package uz.pdp.app_cinema.service.interfaces;
// Bahodir Hasanov 3/16/2022 3:43 PM

import org.springframework.http.HttpEntity;
import uz.pdp.app_cinema.dto.MovieDto;
import uz.pdp.app_cinema.dto.MovieFileDto;

import java.io.IOException;
import java.util.UUID;

public interface MovieService {

    HttpEntity<?> getAllMovies(int size, int page, String search, String sort, boolean direction);

    HttpEntity<?> getMovieById(UUID id);

    //    HttpEntity addMovie(MovieDto movieDto);
    //    HttpEntity editMovie(Integer id, MovieDto movieDto);

    HttpEntity<?> saveMovie(MovieFileDto file, MovieDto movieDto) throws IOException;
}
