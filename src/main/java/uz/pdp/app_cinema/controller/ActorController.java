package uz.pdp.app_cinema.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.app_cinema.model.Actor;
import uz.pdp.app_cinema.model.Attachment;
import uz.pdp.app_cinema.payload.ApiResponse;
import uz.pdp.app_cinema.repository.AttachmentRepository;
import uz.pdp.app_cinema.service.ActorService;

import java.util.UUID;

// Bahodir Hasanov 3/15/2022 3:55 PM
@RestController
@RequestMapping("/api/actors")
public class ActorController {

    @Autowired
    ActorService actorService;


    @GetMapping
    public ResponseEntity<?> getAllActors() {
        ApiResponse apiResponse = actorService.getAllActors();
        return ResponseEntity.status(apiResponse.isStatus() ? HttpStatus.OK : HttpStatus.CONFLICT).body(apiResponse);
    }

    @PostMapping
    public ResponseEntity<?> saveActor(@RequestPart("file") MultipartFile file, @RequestPart("actor") Actor actor) {
        ApiResponse apiResponse = actorService.saveActor(file, actor);
        return ResponseEntity.status(apiResponse.isStatus() ? HttpStatus.OK : HttpStatus.CONFLICT).body(apiResponse);
    }

    @DeleteMapping("/{actorId}")
    public ResponseEntity<?> deleteActor(@PathVariable("actorId") UUID actorId) {
        return actorService.deleteActor(actorId);
    }
}
