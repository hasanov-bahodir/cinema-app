package uz.pdp.app_cinema.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.app_cinema.dto.TicketDto;
import uz.pdp.app_cinema.service.TransactionService;

import java.util.List;
import java.util.UUID;

// Bahodir Hasanov 3/30/2022 11:55 AM

@RestController
@RequiredArgsConstructor
public class TransactionController {
    private final TransactionService transactionService;

    @RequestMapping("api/purchase")
    public HttpEntity<?> createStripeSession() {
        return transactionService.createStripeSession();
    }

    @RequestMapping(value = "api/refund",method = RequestMethod.POST)
    public HttpEntity<?> refundTicket(@RequestBody List<UUID> ticketIds) {
       return transactionService.refundTicket(ticketIds);
    }

}
