package uz.pdp.app_cinema.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.app_cinema.model.Genre;
import uz.pdp.app_cinema.payload.ApiResponse;
import uz.pdp.app_cinema.service.GenreService;

import java.util.UUID;

// Bahodir Hasanov 3/16/2022 2:00 PM
@RestController
@RequestMapping("/api/genres")
public class GenreController {
    @Autowired
    GenreService genreService;

    @GetMapping
    public ResponseEntity<?> getAllGenres() {
        ApiResponse apiResponse = genreService.getAllGenres();
        return ResponseEntity.status(apiResponse.isStatus() ? 200 : 400).body(apiResponse);
    }

    @PostMapping
    private HttpEntity<?> savaGenre(@RequestBody Genre genre) {
        ApiResponse apiResponse = genreService.saveGenre(genre);
        return ResponseEntity.status(apiResponse.isStatus() ? 200 : 409).body(apiResponse);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getGenreById(@PathVariable UUID id) {
        ApiResponse apiResponse = genreService.getGenreById(id);
        return ResponseEntity.status(apiResponse.isStatus() ? 200 : 404).body(apiResponse);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> getDeleteById(@PathVariable UUID id) {
        ApiResponse apiResponse = genreService.getDeleteById(id);
        return ResponseEntity.status(apiResponse.isStatus() ? 200 : 409).body(apiResponse);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> getUpdateById(@PathVariable UUID id, @RequestBody Genre genre) {
        ApiResponse apiResponse = genreService.update(id,genre);
        return ResponseEntity.status(apiResponse.isStatus() ? 200 : 409).body(apiResponse);
    }
}
