package uz.pdp.app_cinema.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.app_cinema.dto.RefundFeeDto;
import uz.pdp.app_cinema.service.RefundChargeFeeService;
import static uz.pdp.app_cinema.utils.Constants.DEFAULT_PAGE_SIZE;


// Bahodir Hasanov 4/5/2022 9:32 AM
@RestController
@RequestMapping("/api/re-cha-fee")
public class RefundChargeFeeController {

    @Autowired
    RefundChargeFeeService refundChargeFeeService;

    @PostMapping
    public ResponseEntity<?> saveRefundChargeFee(@RequestBody RefundFeeDto refundFeeDto){
       return refundChargeFeeService.saveRefundChargeFee(refundFeeDto);
    }

    @GetMapping
    public ResponseEntity<?> getRefundChargeFee(
            @RequestParam (name = "size", defaultValue = DEFAULT_PAGE_SIZE) int size,
            @RequestParam (name = "page", defaultValue = "1") int page
    ){
        return refundChargeFeeService.getRefundChargeFee(page,size);
    }
}
