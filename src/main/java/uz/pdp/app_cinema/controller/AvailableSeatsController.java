package uz.pdp.app_cinema.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.app_cinema.payload.ApiResponse;
import uz.pdp.app_cinema.projection.AvailableSeatsProjection;
import uz.pdp.app_cinema.repository.SeatsRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

// Bahodir Hasanov 3/26/2022 10:34

@RestController
@RequestMapping("/api/seat/available-seat")
public class AvailableSeatsController {
    @Autowired
    SeatsRepository seatsRepository;

    @GetMapping("/{movieSessionId}")
    public ResponseEntity<?> getAllAvailableSeatsByMovieSessionId(@PathVariable UUID movieSessionId) {
        Optional<AvailableSeatsProjection> optionalAvailableSeats = seatsRepository.findAllAvailableSeatsByMovieSessionId(movieSessionId);
        if (optionalAvailableSeats.isPresent()) {
            return ResponseEntity.ok(optionalAvailableSeats.get());
        }
     return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ApiResponse("movie session not fount",false));
    }


}
