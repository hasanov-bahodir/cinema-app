package uz.pdp.app_cinema.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uz.pdp.app_cinema.model.Distributor;
import uz.pdp.app_cinema.payload.ApiResponse;
import uz.pdp.app_cinema.service.DistributorService;

import java.util.UUID;

// Bahodir Hasanov 3/15/2022 10:32 PM
@RestController
@RequestMapping("/api/distributors")
public class DistributorController {
    @Autowired
    DistributorService distributorService;

    @GetMapping
    public HttpEntity<?> getAllDistributors() {
        ApiResponse apiResponse = distributorService.getAllDistributors();
        return ResponseEntity.status(apiResponse.isStatus() ? 200 : 404).body(apiResponse);
    }

    @PostMapping
    public ResponseEntity<?> saveDistributor(@RequestBody Distributor distributor) {
        ApiResponse apiResponse = distributorService.savaDistributor(distributor);
        return ResponseEntity.status(apiResponse.isStatus() ? 200 : 409).body(apiResponse);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getDistributorById(@PathVariable UUID id) {
        ApiResponse apiResponse = distributorService.getDistributorById(id);
        return ResponseEntity.status(apiResponse.isStatus() ? 200 : 404).body(apiResponse);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateDistributorById(@PathVariable UUID id, @RequestBody Distributor distributor) {
        ApiResponse apiResponse = distributorService.updateDistributorById(id,distributor);
        return ResponseEntity.status(apiResponse.isStatus() ? 200 : 404).body(apiResponse);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteDistributor(@PathVariable UUID id){
        ApiResponse apiResponse = distributorService.deleteDistributor(id);
        return ResponseEntity.status(apiResponse.isStatus() ? 200 : 404).body(apiResponse);
    }
}
