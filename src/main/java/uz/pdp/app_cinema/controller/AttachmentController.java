package uz.pdp.app_cinema.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.app_cinema.model.Attachment;
import uz.pdp.app_cinema.payload.ApiResponse;
import uz.pdp.app_cinema.service.AttachmentService;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

// Bahodir Hasanov 3/15/2022 4:07 PM
@RestController
@RequestMapping("/api/attachment")
public class AttachmentController {

    @Autowired
    AttachmentService attachmentService;

    @PostMapping()
    public HttpEntity<?> addAttachment(@RequestParam MultipartFile file) {
        ApiResponse apiResponse = attachmentService.addAttachment(file);
        return ResponseEntity.status(apiResponse.isStatus() ? 200 : 409).body(apiResponse);
    }

    @GetMapping()
    public ResponseEntity<?> getAllAttachment() {
        ApiResponse apiResponse = attachmentService.getAllAttachment();
        return ResponseEntity.status(apiResponse.isStatus() ? 200 : 404).body(apiResponse);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getAttachmentById(@PathVariable UUID id) {
        return attachmentService.getAttachmentById(id);
    }

    @PutMapping("/{id}")
    public HttpEntity<?> updateAttachment(@PathVariable UUID id, @RequestParam MultipartFile file) {
        ApiResponse apiResponse = attachmentService.updateAttachment(id, file);
        return ResponseEntity.status(apiResponse.isStatus() ? 200 : 409).body(apiResponse);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteAttachment(@PathVariable UUID id) {
        ApiResponse apiResponse = attachmentService.deleteAttachment(id);
        return ResponseEntity.status(apiResponse.isStatus() ? 200 : 404).body(apiResponse);
    }

    @GetMapping("/qrCode/{movieSessionId}")
    public ResponseEntity<?> getTicketWithQR(@PathVariable UUID movieSessionId) {
        Attachment attachment = attachmentService.getTicketWithQR(movieSessionId);
        return ResponseEntity.ok(new ApiResponse("saved", true, attachment));
    }
}
