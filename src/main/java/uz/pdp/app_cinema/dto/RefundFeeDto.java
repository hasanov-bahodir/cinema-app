package uz.pdp.app_cinema.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

// Bahodir Hasanov 4/5/2022 9:35 AM
@AllArgsConstructor
@NoArgsConstructor
@Data
public class RefundFeeDto {
    private Integer intervalMinutes;
    private Double percentage;
}
