package uz.pdp.app_cinema.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.UUID;

// Bahodir Hasanov 3/16/2022 3:47 PM
@AllArgsConstructor
@NoArgsConstructor
@Data
public class MovieDto {

    private String title;

    private String description;

    private int durationInMin;

    private double minPrice;

//    private List<MultipartFile> photos;

//    private MultipartFile coverImgId;

    private String trailerVideoUrl; // ex. youtube link

    private LocalDate releaseDate;

    private double budget;

    private UUID distributorId;

    private double distributorShareInPercentage;

    private List<UUID> casts;

    private List<UUID> genres;

}
