package uz.pdp.app_cinema.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.app_cinema.model.template.AbsEntity;

import javax.persistence.*;
import java.util.List;

// Bahodir Hasanov 3/14/2022 6:20 PM
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "hall_rows")
@EqualsAndHashCode(callSuper = true)
public class Row extends AbsEntity {
    private Integer number;
    @OneToMany(mappedBy = "row", cascade = CascadeType.ALL)
    private List<Seat> seats;
    @ManyToOne
    private Hall hall;

    public Row(Integer number, Hall hall) {
        this.number = number;
        this.hall = hall;
    }

}
