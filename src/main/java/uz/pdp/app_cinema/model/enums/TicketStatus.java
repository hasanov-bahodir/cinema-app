package uz.pdp.app_cinema.model.enums;

public enum TicketStatus {
    NEW,
    PURCHASED,
    REFUNDED
}
