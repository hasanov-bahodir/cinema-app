package uz.pdp.app_cinema.model;
// Bahodir Hasanov 3/17/2022 11:31 AM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.app_cinema.model.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity(name = "movies_announcements")
public class MovieAnnouncement extends AbsEntity {
    @OneToOne
    public Movie movie;
    public boolean isActive;
}
