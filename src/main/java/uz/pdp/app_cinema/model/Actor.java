package uz.pdp.app_cinema.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import uz.pdp.app_cinema.model.template.AbsEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

// Bahodir Hasanov 3/14/2022 11:34 PM
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "actors")
@EqualsAndHashCode(callSuper = true)
public class Actor extends AbsEntity {
    @Column(nullable = false, length = 50)
    @NotNull
    private String fullName;
    @OneToOne(cascade =CascadeType.ALL)
    private Attachment attachment;
}
