package uz.pdp.app_cinema.model;

// Bahodir Hasanov 3/15/2022 7:07 AM

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.app_cinema.model.template.AbsEntity;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
//@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "attachments")
public class Attachment extends AbsEntity {

    @Column(nullable = false)
    private String content_type;

    @Column(nullable = false)
    private Long size;

    @Column(nullable = false)
    private String name;

    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY,mappedBy = "attachment", cascade = CascadeType.ALL)
    private AttachmentContent attachmentContent;


    public Attachment(String content_type, Long size, String name) {
        this.content_type = content_type;
        this.size = size;
        this.name = name;
    }
}

