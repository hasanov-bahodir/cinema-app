package uz.pdp.app_cinema.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.app_cinema.model.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

// Bahodir Hasanov 3/17/2022 1:05 PM
@AllArgsConstructor
@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@Entity(name = "night_session_add_fee")
public class NightSessionAddFee extends AbsEntity {
    private Double percentage;
    @OneToOne
    private SessionTime time;
}
