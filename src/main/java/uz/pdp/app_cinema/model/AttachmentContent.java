package uz.pdp.app_cinema.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import uz.pdp.app_cinema.model.template.AbsEntity;

import javax.persistence.*;

// Bahodir Hasanov 3/15/2022 7:07 AM

@AllArgsConstructor
@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@Entity(name = "attachment_contents")
public class AttachmentContent extends AbsEntity {
    @OneToOne()
//    @OnDelete(action = OnDeleteAction.CASCADE)
    private Attachment attachment;
    private byte[] bytes;
}
