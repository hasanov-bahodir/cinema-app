package uz.pdp.app_cinema.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.EnableMBeanExport;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

// Bahodir Hasanov 3/15/2022 8:56 AM
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class WaitingPurchaseTime {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Long time;
}
