package uz.pdp.app_cinema.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.app_cinema.model.enums.TicketStatus;
import uz.pdp.app_cinema.model.template.AbsEntity;

import javax.persistence.*;

// Bahodir Hasanov 3/15/2022 7:16 AM
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "tickets")
public class Ticket extends AbsEntity {
    @ManyToOne
    private MovieSession movieSession;

    @ManyToOne
    private Seat seat;

    @OneToOne(cascade = CascadeType.ALL)
    private Attachment qrCode;

    private Double price;

    @Enumerated(value = EnumType.STRING)
    private TicketStatus status = TicketStatus.NEW;

    @ManyToOne
    private User user;
}
