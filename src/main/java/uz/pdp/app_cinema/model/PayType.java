package uz.pdp.app_cinema.model;

// Bahodir Hasanov 3/15/2022 8:53 AM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.app_cinema.model.template.AbsEntity;

import javax.persistence.Entity;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class PayType extends AbsEntity {
    private Double commissionFeeInPercent;
}
